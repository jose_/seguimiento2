<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'intermedio')">Intermedio</button>
  <button class="tablinks" onclick="openCity(event, 'fin')">Fin de año</button>
</div>
<!-- Tab content -->
<div id="intermedio" class="tabcontent">

  <div class="panel">
    <div class="tab-title clearfix">
        <div class="display">
        <h4>Proceso Intermedio</h4>
        <div class="title-button-group">
            <?php
            
            if ($can_create_milestones) {
                echo modal_anchor(get_uri("projects/milestone_modal_form"), "<i class='fa fa-plus-circle'></i> " . lang('add_milestone'), array("class" => "btn btn-default", "title" => lang('add_milestone'), "data-post-project_id" => $project_id));
            }
        
            ?> 
        </div>
    </div>

    <div class="table-responsive">
        <table id="milestone-table" class="display" width="100%">            
        </table>
    </div>
</div>
</div>
</div>

<div id="fin" class="tabcontent">
  <div class="panel">
    <div class="tab-title clearfix">
                <div class="display">

        <h4>Proceso Fin de Año</h4>
        <div class="title-button-group">
            <?php
            
            if ($can_create_milestones) {
                echo modal_anchor(get_uri("projects/milestone_modal_form"), "<i class='fa fa-plus-circle'></i> " . lang('add_milestone'), array("class" => "btn btn-default", "title" => lang('add_milestone'), "data-post-project_id" => $project_id));
            }
        
            ?> 
        </div>
    </div>

    <div class="table-responsive">
      <table id="milestone-table2" class="display" width="100%">            
        </table>
    </div>
</div>
</div>
</div>
<style>

/* Style the tab */
.tab {
    overflow: hidden;
   }

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    border-top: none;
}

.tabcontent#intermedio {
    display: block;
    border-top: none;
}

.tabcontent .display {
    display: block;
}



</style>
<script type="text/javascript">
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>




<script type="text/javascript">
    $(document).ready(function () {
        var optionVisibility = false;
        if ("<?php echo ($can_edit_milestones || $can_delete_milestones); ?>") {
            optionVisibility = true;
        }
        $("#milestone-table").appTable({
            source: '<?php echo_uri("projects/milestones_list_data/" . $project_id) ?>',
            order: [[0, "desc"]],
            columns: [
                {visible: false, searchable: false},
                {title: "<?php echo lang("title") ?><img class='icono-tabla' src='../../../assets/images/titulo.png'>"},
                {title: "<?php echo "Resultado Esperado"; ?>  <img class='icono-tabla' src='../../../assets/images/buscar.png'>" },
                {title: "<?php echo "Aprendizaje"; ?><img class='icono-tabla' src='../../../assets/images/aprendizaje.png'>"},
                {title: "<?php echo "Desafio"; ?><img class='icono-tabla' src='../../../assets/images/desafio.png'>"},
                {visible: optionVisibility, title: '<i class="fa fa-bars"></i>', "class": "text-center option w100"}
            ],
            printColumns: [1, 2, 3, 4,5]
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {

        // Entrar al menú procesos por defecto --ok

            $("a[data-target='#project-milestones-section']:first").trigger("click")
        //

        var optionVisibility = false;
        if ("<?php echo ($can_edit_milestones || $can_delete_milestones); ?>") {
            optionVisibility = true;
        }
        $("#milestone-table2").appTable({
            source: '<?php echo_uri("projects/milestones_list_data2/" . $project_id) ?>',
            order: [[0, "desc"]],
            columns: [
                {visible: false, searchable: false},
                {title: "<?php echo lang("title") ?><img class='icono-tabla'src='../../../assets/images/titulo.png'>"},
                {title: "<?php echo "Resultado esperado"; ?><img class='icono-tabla' src='../../../assets/images/buscar.png'>"},
                 {title: "<?php echo "Resultados obtenidos"; ?><img class='icono-tabla' src='../../../assets/images/resultadosPS_8nov.png'>"},
                {title: "<?php echo "Aprendizajes"; ?><img class='icono-tabla' src='../../../assets/images/aprendizaje.png'>"},
                {title: "<?php echo "Desafios"; ?><img class='icono-tabla' src='../../../assets/images/desafio.png'>"},
                 {title: "<?php echo "Retos"; ?><img class='icono-tabla' src='../../../assets/images/retosPS_8nov.png'>"},


                {visible: optionVisibility, title: '<i class="fa fa-bars"></i>', "class": "text-center option w100"}
            ],
            printColumns: [1, 2, 3, 4,5]
        });

        
       // $(".tablinks:first").trigger("click")
    });
</script>