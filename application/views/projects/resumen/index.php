<script type="text/javascript">
(function (jQuery) {

    jQuery.fn.ganttView = function () {

        var args = Array.prototype.slice.call(arguments);
        if (args.length == 1 && typeof (args[0]) == "object") {
            build.call(this, args[0]);
        }

        if (args.length == 2 && typeof (args[0]) == "string") {
            handleMethod.call(this, args[0], args[1]);
        }
    };
    function build(options) {

        var els = this;
        var defaults = {
            vHeaderWidth: 100,
            
           
        };
        var opts = jQuery.extend(true, defaults, options);
        if (opts.data) {
            build();
        } else if (opts.dataUrl) {
            jQuery.getJSON(opts.dataUrl, function (data) {
                opts.data = data;
        //         var jose=document.getElementById('whereToPrint').innerHTML = JSON.stringify(data, null, 4);
                build();
            });
        }

        function build() {

            var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 5);
            els.each(function () {

                var container = jQuery(this);
                jQuery(container).html("");
                var div = jQuery("<div>", {"class": "ganttview"});
                container.append(div);
                new Chart(div, opts).render();
                var w = jQuery("div.ganttview-vtheader", container).outerWidth();
                // container.css("width", (w + 2) + "px");
                //new Behavior(container, opts).apply();
            });
        }
    }



    var Chart = function (div, opts) {

        function render() {
            addVtHeader(div, opts.data, opts.cellHeight);
        }


        // Creates a 3 dimensional array [year][month][day] of every day 
        // between the given start and end dates
        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<section>",{"class": "contenedor"});
var nombre = data["0"].series["0"].asigando;
             document.getElementById("demo").innerHTML =nombre;
            for (var i = 0; i < data.length; i++) {
                //var itemDiv = jQuery("<div>", {"class": "ganttview-vtheader-series"});
                var itemDiv= jQuery("<div>", {"id": "asd"          }).append("  	<section id='contenido'><b>Proceso Numero </b>" + [i+1]+"</section>	<section id='middlejose'><b> Nombre del proceso: </b>"+data[i].name+"</section><b>	<section id='sidebarjose'>Descripción del proceso: </b>"+data[i].desc+"</section><br>	<section id='contenido2'>Aprendizaje: "+data[i].aprendizajes+"</section>	<section id='middlejose2'>Desafios:"+data[i].desafios+"</section>");
                  var seriesDiv = jQuery("<table><tr><th>Actividad</th><th>Descripcion</th><th>Resultados</th><th>Otros Particpantes</th><th>Areas</th></tr>", {"class": "ganttview-vtheader-series row-group-" + i});
                for (var j = 0; j < data[i].series.length; j++) {
                    seriesDiv.append("<tr><td>"+[j+1]+".-"+data[i].series[j].name+"</td><td>"+data[i].series[j].descripcion+"</td><td>"+data[i].series[j].resultado+"</td><td>"+data[i].series[j].otros+"</td><td>"+data[i].series[j].cola+"</tr></td>");
                }
                itemDiv.append(seriesDiv);
               headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }


        function addGrid(div, data, dates, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", {"class": "ganttview-grid"});
            var rowDiv = jQuery("<div>", {"class": "ganttview-grid-row"});
            for (var y in dates) {
                for (var m in dates[y]) {
                    for (var d in dates[y][m]) {
                        var firstDayClass = "";


                        var cellDiv = jQuery("<div>", {"class": "ganttview-grid-row-cell " + firstDayClass});
                        rowDiv.append(cellDiv);

                        if (d == 0) {
                            cellDiv.prev().addClass("ganttview-last-day");
                        }
                    }
                }
            }
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", w + "px");
            gridDiv.css("width", w + "px");
            for (var i = 0; i < data.length; i++) {
                gridDiv.append(jQuery("<div>", {
                    "class": "ganttview-grid-header"
                }));
                for (var j = 0; j < data[i].series.length; j++) {
                    var newDiv = rowDiv.clone();
                    newDiv.addClass("row-group-" + i);
                    gridDiv.append(newDiv);

                }
            }


            div.append(gridDiv);
        }

     
     
        return {
            render: render
        };
    }

})(jQuery);
 

</script>

<script type="text/javascript">
function printDiv() 
{

  var divToPrint=document.getElementById('imprimir');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}



    var loadGantt = function (group_by, id) {
            	    $('.quitando').find('script').attr('src', 'http://localhost:8822/reciente/seguimiento/assets/js/gantt-chart/gantt.js').remove();

        group_by = group_by || "milestones";
        id = id || "0";

        $("#resumen-chart").html("<div style='height:100px;'></div>");
        appLoader.show({container: "#resumen-chart", css: "right:50%;"});

        $("#resumen-chart").ganttView({
            dataUrl: "<?php echo get_uri("projects/gantt_data_jose/" . $project_id); ?>" + "/" + group_by + "/" + id,
            monthNames: AppLanugage.months,
            dayText: "<?php echo lang('day'); ?>",
            daysText: "<?php echo lang('days'); ?>"
        });
    };

    $(document).ready(function () {
    	    $('.quitando').find('script').attr('src', 'http://localhost:8822/reciente/seguimiento/assets/js/gantt-chart/gantt.js').remove();

        var $resumenGroupBy = $("#resumen-group-by"),
                $resumenMilestone = $("#resumen-milestone-dropdown"),
                $resumenMembers = $("#resumen-members-dropdown");

        $resumenGroupBy.select2();

        $resumenMilestone.select2({
            data: <?php echo $milestone_dropdown; ?>
        });

        if ($resumenMembers.length) {
            $resumenMembers.select2({
                data: <?php echo $project_members_dropdown; ?>
            });
        }

        $(".reload-resumen").change(function () {
            var group_by = $resumenGroupBy.val() || "milestones",
                    id = 0;

            if (group_by === "milestones") {
                $resumenMilestone.removeClass("hide");
                id = $resumenMilestone.val();
                $resumenMembers.addClass("hide");
            } else {
                $resumenMembers.removeClass("hide");
                id = $("#resumen-members-dropdown").val();
                $resumenMilestone.addClass("hide");
            }

            loadGantt(group_by, id);
        });

        loadGantt();
    });
</script>
<style>
 

table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 18px;
}


@import url(https://fonts.googleapis.com/css?family=Open+Sans);


/* STRUCTURE */
#asd { 
    margin:20px;
}
#pagewrapjose {
	padding: 5px;
	width: 960px;
	margin: 20px auto;
}
#contenido {
	width: 290px;
	float: left;
	padding: 5px 15px;
}
#contenido2 {
	width: 470px;
	float: left;
	padding: 5px 15px;
}


#middlejose {
	width: 294px; /* Account for margins + border values */
	float: left;
	padding: 5px 15px;
	margin: 0px 5px 5px 5px;
    background:#f0efef;

}

#middlejose2 {
	width: 474px; /* Account for margins + border values */
	float: left;
	padding: 5px 15px;
	margin: 0px 5px 5px 5px;
	background:#f0efef;
}


#sidebarjose {
	width: 270px;
	padding: 5px 15px;
	float: left;
}



#contenido {
	background: #f0efef;
}

#contenido2 {
	background: #f0efef;
}
#sidebarjose {
	background: #f0efef;
}

#pagewrapjose,  #contenido, #middlejose, #sidebarjose{
	border: solid 1px #ccc;
}

  @import url(http://fonts.googleapis.com/css?family=Droid+Serif);

.contenedor {
  background-color: #ffffff;
  width: 1020px;
  margin: 0 auto;
  overflow: auto;
}
/* One Column */

.column-1 {
  width: 910px;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
}
/* Two Column */

.column-2-left {
  width: 420px;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
  float: left;
  margin-right: 10px;
}

.column-2-right {
  width: 420px;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
  float: right;
  margin-left: 10px;
}
/* Three Column */

.column-3-left {
  width: 256px;

  height:200px;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
  float: left;
  margin-right: 10px;
}

.column-3-middle {
  height: 200px;
  width: 258px;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
  float: left;
  margin-left: 10px;
  margin-right: 10px;
}

.column-3-right {
  width: 256px;
  height:200pxs;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
  float: left;
  margin-left: 10px;
}
/* Four Column */

.column-4-left {
  width: 175px;
  background-color: #565855;
  padding: 20px;
  border: 5px solid #565855;
  float: left;
  margin-right: 10px;
}

.column-4-middle {
  width: 175px;
  background-color: #f5d7ce;
  padding: 20px;
  border: 5px solid #cb785e;
  float: left;
  margin-left: 10px;
  margin-right: 10px;
}

.column-4-right {
  width: 175px;
  background-color: #f5d7ce;
  padding: 20px;
  border: 5px solid #cb785e;
  float: right;
  margin-left: 10px;
}
/* Five Column */

.column-5-left {
  width: 126px;
    background-color: #e7e6f8;
    padding: 20px;
    border: 5px solid #c2beeb;
    float: left;
    margin-left: 20px;
    margin-right: 80px;
}

.column-5-middle {
  width: 126px;
  height: 80px;

  background-color: #e7e6f8;
  padding: 20px;
  border: 5px solid #c2beeb;
  float: left;
  margin-left: 10px;
  margin-right: 10px;
}

.column-5-right {
  width: 126px;
  background-color: #e7e6f8;
  padding: 20px;
  border: 5px solid #c2beeb;
  float: right;
  margin-left: 10px;
}




</style>
        <div class="panel">
<input type='button' id='btn' value='Print' onclick='printDiv();'>

    <div class="tab-title clearfix">
	        
            <div class="pull-right p10 mr10">
            <?php
            if ($show_project_members_dropdown) {
                echo lang("group_by") . " : ";
                echo form_dropdown("resumen-group-by", array("milestones" => lang("milestones"), "members" => lang("team_members")), array(), "class='select2 w200 mr10 reload-resumen' id='resumen-group-by'");

                echo form_input(array(
                    "id" => "resumen-members-dropdown",
                    "name" => "resumen-members-dropdown",
                    "class" => "select2 w200 reload-resumen hide",
                    "placeholder" => lang('team_member')
                ));
            }
            ?>
            <?php
            echo form_input(array(
                "id" => "resumen-milestone-dropdown",
                "name" => "resumen-milestone-dropdown",
                "class" => "select2 w200 reload-resumen",
                "placeholder" => lang('milestone')
            ));
            ?>
        </div>
    </div>
<div id="imprimir">
        <h4>Programa:  
        	<?php 
        	 
 echo $project_info->title; 

         	?>    </h4>

         	<p id="demo"></p>
    
    <div class="w10d0p pt10">
        <div id="resumen-chart" style="width: 100%;"></div>

	</div>
   </div>
</div>
