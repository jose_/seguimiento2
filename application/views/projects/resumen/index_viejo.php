<script type="text/javascript">
(function (jQuery) {

    jQuery.fn.ganttView = function () {

        var args = Array.prototype.slice.call(arguments);
        if (args.length == 1 && typeof (args[0]) == "object") {
            build.call(this, args[0]);
        }

        if (args.length == 2 && typeof (args[0]) == "string") {
            handleMethod.call(this, args[0], args[1]);
        }
    };
    function build(options) {

        var els = this;
        var defaults = {
            vHeaderWidth: 100,
            
           
        };
        var opts = jQuery.extend(true, defaults, options);
        if (opts.data) {
            build();
        } else if (opts.dataUrl) {
            jQuery.getJSON(opts.dataUrl, function (data) {
                opts.data = data;
        //         var jose=document.getElementById('whereToPrint').innerHTML = JSON.stringify(data, null, 4);
                build();
            });
        }

        function build() {

            var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 5);
            els.each(function () {

                var container = jQuery(this);
                jQuery(container).html("");
                var div = jQuery("<div>", {"class": "ganttview"});
                container.append(div);
                new Chart(div, opts).render();
                var w = jQuery("div.ganttview-vtheader", container).outerWidth();
                // container.css("width", (w + 2) + "px");
                //new Behavior(container, opts).apply();
            });
        }
    }



    var Chart = function (div, opts) {

        function render() {
            addVtHeader(div, opts.data, opts.cellHeight);
        }


        // Creates a 3 dimensional array [year][month][day] of every day 
        // between the given start and end dates
        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<div>", {"class": "ganttview-vtheader2"});
            for (var i = 0; i < data.length; i++) {
                var itemDiv = jQuery("<div>", {"class": "ganttview-vtheader-series"});
                itemDiv.append(jQuery("<div>", {
                    "class": "jose"
                }).append("<b>Proceso </b>" + [i+1]+"<b> Proceso: </b>"+data[i].name+"<b>Descripción del proceso: </b>"+data[i].desc+"<br><b>Aprendizaje: </b>"+data[i].aprendizajes+" <b>Desafios:</b>"+data[i].desafios+"<br>"));
                  var seriesDiv = jQuery("<div>", {"class": "ganttview-vtheader-series row-group-" + i});
                for (var j = 0; j < data[i].series.length; j++) {
                    seriesDiv.append(jQuery("<div>", {"class": "ganttview-vtheader-series-row"}).append([j+1]+".-"+data[i].series[j].name).append("<br>Descripcion de la actividad: "+data[i].series[j].descripcion).append("<br>Resultados:"+data[i].series[j].resultado).append("<br>Otros participantes:"+data[i].series[j].otros).append("<br>Otros Areas participantes:"+data[i].series[j].cola));
                }
                itemDiv.append(seriesDiv);
                headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }


        function addGrid(div, data, dates, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", {"class": "ganttview-grid"});
            var rowDiv = jQuery("<div>", {"class": "ganttview-grid-row"});
            for (var y in dates) {
                for (var m in dates[y]) {
                    for (var d in dates[y][m]) {
                        var firstDayClass = "";


                        var cellDiv = jQuery("<div>", {"class": "ganttview-grid-row-cell " + firstDayClass});
                        rowDiv.append(cellDiv);

                        if (d == 0) {
                            cellDiv.prev().addClass("ganttview-last-day");
                        }
                    }
                }
            }
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", w + "px");
            gridDiv.css("width", w + "px");
            for (var i = 0; i < data.length; i++) {
                gridDiv.append(jQuery("<div>", {
                    "class": "ganttview-grid-header"
                }));
                for (var j = 0; j < data[i].series.length; j++) {
                    var newDiv = rowDiv.clone();
                    newDiv.addClass("row-group-" + i);
                    gridDiv.append(newDiv);

                }
            }


            div.append(gridDiv);
        }

     
     
        return {
            render: render
        };
    }

})(jQuery);
 

</script>

<script type="text/javascript">
    var loadGantt = function (group_by, id) {
        group_by = group_by || "milestones";
        id = id || "0";

        $("#resumen-chart").html("<div style='height:100px;'></div>");
        appLoader.show({container: "#resumen-chart", css: "right:50%;"});

        $("#resumen-chart").ganttView({
            dataUrl: "<?php echo get_uri("projects/gantt_data_jose/" . $project_id); ?>" + "/" + group_by + "/" + id,
            monthNames: AppLanugage.months,
            dayText: "<?php echo lang('day'); ?>",
            daysText: "<?php echo lang('days'); ?>"
        });
    };

    $(document).ready(function () {
        var $ganttGroupBy = $("#resumen-group-by"),
                $ganttMilestone = $("#resumen-milestone-dropdown"),
                $ganttMembers = $("#gantt-members-dropdown");

        $ganttGroupBy.select2();

        $ganttMilestone.select2({
            data: <?php echo $milestone_dropdown; ?>
        });

        if ($ganttMembers.length) {
            $ganttMembers.select2({
                data: <?php echo $project_members_dropdown; ?>
            });
        }

        $(".reload-gantt").change(function () {
            var group_by = $ganttGroupBy.val() || "milestones",  id = 0;

            if (group_by === "milestones") {
                $ganttMilestone.removeClass("hide");
                id = $ganttMilestone.val();
                $ganttMembers.addClass("hide");
            } else {
                $ganttMembers.removeClass("hide");
                id = $("#gantt-members-dropdown").val();
                $ganttMilestone.addClass("hide");
            }

            loadGantt(group_by, id);
        });

        loadGantt();
    });
</script>
        <div class="panel">
    <div class="tab-title clearfix">
        <h4><?php echo lang('gantt'); ?></h4>
            <div class="pull-right p10 mr10">
            <?php
            if ($show_project_members_dropdown) {
                echo lang("group_by") . " : ";
                echo form_dropdown("resumen-group-by", array("milestones" => lang("milestones"), "members" => lang("team_members")), array(), "class='select2 w200 mr10 reload-gantt' id='resumen-group-by'");

                echo form_input(array(
                    "id" => "gantt-members-dropdown",
                    "name" => "gantt-members-dropdown",
                    "class" => "select2 w200 reload-gantt hide",
                    "placeholder" => lang('team_member')
                ));
            }
            ?>
            <?php
            echo form_input(array(
                "id" => "resumen-milestone-dropdown",
                "name" => "resumen-milestone-dropdown",
                "class" => "select2 w200 reload-gantt",
                "placeholder" => lang('milestone')
            ));
            ?>
        </div>
    </div>
    <div class="w10d0p pt10">
        <div id="resumen-chart" style="width: 100%;"></div>
    </div>

</div>

