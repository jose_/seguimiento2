
<?php echo form_open(get_uri("projects/save_milestone3"), array("id" => "milestone-form", "class" => "general-form", "role" => "form")); ?>
<div class="modal-body clearfix">
          <img src="../../../assets/images/resultados.png" style="width:200px;height: 200px;" class="center" >

    <input type="hidden" name="id" value="<?php echo $model_info->id; ?>" />
    <input type="hidden" name="project_id" value="<?php echo $project_id; ?>" />
    <div class="form-group"style="display:none" >
        <label for="title" class=" col-md-3"><b><?php echo lang('title'); ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "title",
                "name" => "title",
                "value" => $model_info->title,
                "class" => "form-control",
                "placeholder" => lang('title'),
                "maxlength" => "500",
                "autofocus" => true,
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
            ));
            ?>
        </div>
    </div>
    <div class="form-group">
        <label for="resultados_obtenidos1" class=" col-md-3"><b><?php echo "Seleciona Categoria"; ?></b></label>
        <div class=" col-md-9">
                <?php
                echo form_dropdown("resultados_obtenidos1", array("Modificaciones en marcos normativos." =>"Modificaciones en marcos normativos.", "Modificaciones en diseños y prácticas institucionales."=>"Modificaciones en diseños y prácticas institucionales.", "Modificaciones en el presupuesto"=>"Modificaciones en el presupuesto","Fortalecimiento de capacidades"=>"Fortalecimiento de capacidades","Investigación y generación de evidencias"=>"Investigación y generación de evidencias","Generación de redes y alianzas"=>"Generación de redes y alianzas"), array($model_info->resultados_obtenidos1), "class='form-control'");
                ?>
        </div>
        
    </div>
    <div class="form-group">
        <label for="resultados_obtenidos2" class=" col-md-3"><b><?php echo ""; ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "resultados_obtenidos2",
                "name" => "resultados_obtenidos2",
				//cambiando para este segunda periodo captura
                "value" => $model_info->resultados_obtenidos2,
                "class" => "form-control",
                "maxlength" => "500",
                "data-rule-required" => true,
                "data-msg-required" => "No dejar en blanco",
                "placeholder" => ""
            ));
            ?>
        </div>
    </div>


  <div class="form-group">
        <label for="resultados_obtenidos3" class=" col-md-3"><b><?php echo "Seleciona Categoria"; ?></b></label>
        <div class=" col-md-9">
                <?php
                echo form_dropdown("resultados_obtenidos3", array("Modificaciones en marcos normativos." =>"Modificaciones en marcos normativos.", "Modificaciones en diseños y prácticas institucionales."=>"Modificaciones en diseños y prácticas institucionales.", "Modificaciones en el presupuesto"=>"Modificaciones en el presupuesto","Fortalecimiento de capacidades"=>"Fortalecimiento de capacidades","Investigación y generación de evidencias"=>"Investigación y generación de evidencias","Generación de redes y alianzas"=>"Generación de redes y alianzas"),array($model_info->resultados_obtenidos3), "class='form-control'");
                ?>
        </div>
        
    </div>
    <div class="form-group">
        <label for="resultados_obtenidos4" class=" col-md-3"><b><?php echo ""; ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "resultados_obtenidos4",
                "name" => "resultados_obtenidos4",
                //cambiando para este segunda periodo captura
                "value" => $model_info->resultados_obtenidos4,
                "class" => "form-control",
                "maxlength" => "500",
                "data-rule-required" => true,
                "data-msg-required" => "No dejar en blanco",
                "placeholder" => ""
            ));
            ?>
        </div>
    </div>
  <div class="form-group">
        <label for="resultados_obtenidos5" class=" col-md-3"><b><?php echo "Seleciona categoria"; ?></b></label>
        <div class=" col-md-9">
                <?php
                echo form_dropdown("resultados_obtenidos5", array("Modificaciones en marcos normativos." =>"Modificaciones en marcos normativos.", "Modificaciones en diseños y prácticas institucionales."=>"Modificaciones en diseños y prácticas institucionales.", "Modificaciones en el presupuesto"=>"Modificaciones en el presupuesto","Fortalecimiento de capacidades"=>"Fortalecimiento de capacidades","Investigación y generación de evidencias"=>"Investigación y generación de evidencias","Generación de redes y alianzas"=>"Generación de redes y alianzas") , array($model_info->resultados_obtenidos5), "class='form-control'");
                ?>
        </div>
        
    </div>
    <div class="form-group">
        <label for="resultados_obtenidos6" class=" col-md-3"><b><?php echo ""; ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "resultados_obtenidos6",
                "name" => "resultados_obtenidos6",
                //cambiando para este segunda periodo captura
                "value" => $model_info->resultados_obtenidos6,
                "class" => "form-control",
                "maxlength" => "500",
                "data-rule-required" => true,
                "data-msg-required" => "No dejar en blanco",
                "placeholder" => ""
            ));
            ?>
        </div>
    </div>
    <div class="form-group" style="display:none">
        <label for="description" class=" col-md-3"><b><?php echo lang('description'); ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "description",
                "name" => "description",
                "value" => $model_info->description,
                "class" => "form-control",
                "placeholder" => lang('description')
            ));
            ?>
        </div>
    </div>
    <div class="form-group" style="display:none">
        <label for="due_date" class=" col-md-3"><b><?php echo lang('due_date'); ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "due_date",
                "name" => "due_date",
                "value" => $model_info->due_date,
                "class" => "form-control",
                "placeholder" => lang('due_date'),
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required")
            ));
            ?>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> <?php echo lang('close'); ?></button>
    <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> <?php echo lang('save'); ?></button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#milestone-form").appForm({
            onSuccess: function (result) {
                $("#milestone-table").appTable({newData: result.data, dataId: result.id});
            }
        });
        $("#title").focus();

        setDatePicker("#due_date");

    });
</script>    