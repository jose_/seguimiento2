      <img src="../../../assets/images/actividad.png" style="width:150px;height: 150px;" class="center" >

<?php echo form_open(get_uri("projects/save_task"), array("id" => "task-form", "class" => "general-form", "role" => "form")); ?>
<div class="modal-body clearfix">
    <input type="hidden" name="id" value="<?php echo $model_info->id; ?>" />
    <input type="hidden" name="project_id" value="<?php echo $project_id; ?>" />
    <div class="form-group">
        <label for="title" class=" col-md-3"><b><?php echo lang('title'); ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "title",
                "name" => "title",
                "value" => $model_info->title,
                "class" => "form-control",
                "placeholder" => lang('title'),
                "autofocus" => true,
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
            ));
            ?>
        </div>
    </div>
      
    <div class="form-group">
        <label for="description" class=" col-md-3"><b><?php echo lang('description'); ?></label></b></label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "description",
                "name" => "description",
                "value" => $model_info->description,
                "class" => "form-control",
                "placeholder" => lang('description'),
                "maxlength" => "300",
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required")
            ));
            ?>
        </div>
    </div>
     <div class="form-group">
      <!-- 
        <label for="points" class="col-md-3"><?php// echo lang('points'); ?>
            <span class="help" data-toggle="tooltip" title="<?php //echo lang('task_point_help_text'); ?>"><i class="fa fa-question-circle"></i></span>
        </label>
  
        <div class="col-md-9">
            <?php
            //echo form_dropdown("points", $points_dropdown, array($model_info->points), "class='select2'");
            ?>
        </div>
             -->
    </div>
   
    <div class="form-group">
        <label for="milestone_id" class=" col-md-3"><b><?php echo lang('milestone'); ?></b></label>
        <div class="col-md-9">
              
<textarea rows="4" cols="50"  class="form-control" disabled>
    <?php echo $projectos; ?> 
</textarea>
    <input class="form-control" id="milestone_id" type="hidden" name="milestone_id" value= "<?php echo $idproyectos; ?>">


        </div>
    </div>

    <?php if ($show_assign_to_dropdown) { ?>
        <div class="form-group">
            <label for="assigned_to" class=" col-md-3"><b><?php echo lang('assign_to'); ?></b></label>
            <div class="col-md-9">
                <?php
                echo form_dropdown("assigned_to", $assign_to_dropdown, array($model_info->assigned_to), "class='select2'");
                ?>
            </div>
        </div>



        <div class="form-group">
            <div class="form-group">
            <label for="assigned_to" class=" col-md-3"><b>Otros</b></label>
            <div class="col-md-9">
             <input type="text" name="otros" value="" id="otros" class="form-control" placeholder="Escribe otros participante" >

            </div>
        </div>



        <div class="form-group">
       <!--      
            <label for="collaborators" class=" col-md-3"><?php //echo lang('collaborators'); ?></label>
            <div class="col-md-9">
                <?php /*
               echo form_input(array(
                    "id" => "collaborators",
                    "name" => "collaborators",
                    "value" => $model_info->collaborators,
                    "class" => "form-control",
                    "placeholder" => lang('collaborators')
                ));
             */ ?>
            </div>
        
            -->
        </div>

    <?php } ?>

    <div class="form-group" style="display:none">
     
        <label for="status" class=" col-md-3"><b><?php echo lang('status'); ?></b></label>
        <div class="col-md-9" >
            <?php
            $task_status = array("to_do" => lang('to_do'), "in_progress" => lang('in_progress'));
            echo form_dropdown("status", $task_status, array($model_info->status), "class='select2'");
            ?>
        </div>
    </div>
    <div class="form-group">
             <!--Ignorando puntos 
      
        <label for="project_labels" class=" col-md-3"><?php //echo lang('labels'); ?></label>
        <div class=" col-md-9">
            <?php
       /*     echo form_input(array(
                "id" => "project_labels",
                "name" => "labels",
                "value" => $model_info->labels,
                "class" => "form-control",
                "placeholder" => lang('labels')
            ));
         */   ?>
        </div>
    -->
    </div>
    <div class="form-group">
        <label for="start_date" class=" col-md-3"><b><?php echo lang('start_date'); ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "start_date",
                "name" => "start_date",
                "value" => $model_info->start_date * 1 ? $model_info->start_date : "",
                "class" => "form-control",
                "placeholder" => "YYYY-MM-DD"
            ));
            ?>
        </div>
    </div>
    <div class="form-group">
        <label for="deadline" class=" col-md-3"><b><?php echo lang('deadline'); ?></b></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "deadline",
                "name" => "deadline",
                "value" => $model_info->deadline * 1 ? $model_info->deadline : "",
                "class" => "form-control",
                "placeholder" => "YYYY-MM-DD"
            ));
            ?>
        </div>
    </div>
    <?php $this->load->view("custom_fields/form/prepare_context_fields", array("custom_fields" => $custom_fields, "label_column" =>"col-md-3", "field_column" => " col-md-9")); ?> 
    

  <div class="form-group">
        
            <div id="project-file-dropzone" class="dropzone mb15">

            </div>
            <div id="project-file-dropzone-scrollbar">
                <div id="project-file-previews">
                    <div id="project-file-upload-row" class="box">
                        <div class="preview box-content pr15" style="width:100px;">
                            <img data-dz-thumbnail class="upload-thumbnail-sm" />
                            <div class="progress progress-striped upload-progress-sm active mt5" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                        </div>
                        <div class="box-content">
                            <p class="name" data-dz-name></p>
                            <p class="clearfix">
                                <span class="size pull-left" data-dz-size></span>
                                <span data-dz-remove class="btn btn-default btn-sm border-circle pull-right mt-5 mr10">
                                    <i class="fa fa-times"></i>
                                </span>
                            </p>
                            <strong class="error text-danger" data-dz-errormessage></strong>
                            <input class="file-count-field" type="hidden" name="files[]" value="" />
                            <input class="form-control description-field" type="text" style="cursor: auto;" placeholder="<?php echo lang("description") ?>" />
                        </div>
                    </div>
                </div>
            </div>
        
    </div>


</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> <?php echo lang('close'); ?></button>
    <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> <?php echo lang('save'); ?></button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#task-form").appForm({
            onSuccess: function (result) {
                $("#task-table").appTable({newData: result.data, dataId: result.id});
            }
        });
        $("#task-form .select2").select2();
        $("#title").focus();

        setDatePicker("#start_date, #end_date, #deadline");

        $("#project_labels").select2({
            tags: <?php echo json_encode($label_suggestions); ?>
        });

        $("#collaborators").select2({
            tags: <?php echo json_encode($collaborators_dropdown); ?>
        });

        $('[data-toggle="tooltip"]').tooltip();

    });
</script>    

<script type="text/javascript">
    $(document).ready(function () {
        fileSerial = 0;
        $("#file-form").appForm({
            onSuccess: function (result) {
                $("#project-file-table").appTable({reload: true});
            }
        });
        $("#title").focus();

        setDatePicker("#start_date, #end_date");

        // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
        var previewNode = document.querySelector("#project-file-upload-row");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var projectFilesDropzone = new Dropzone("#project-file-dropzone", {
            url: "<?php echo get_uri("projects/upload_file"); ?>",
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            maxFilesize: 3000,
            previewTemplate: previewTemplate,
            dictDefaultMessage: '<?php echo lang("file_upload_instruction"); ?>',
            autoQueue: true,
            previewsContainer: "#project-file-previews",
            clickable: true,
            accept: function (file, done) {

                if (file.name.length > 200) {
                    done("Filename is too long.");
                    $(file.previewTemplate).find(".description-field").remove();
                }

                //validate the file?
                $.ajax({
                    url: "<?php echo get_uri("projects/validate_project_file"); ?>",
                    data: {file_name: file.name, file_size: file.size},
                    cache: false,
                    type: 'POST',
                    dataType: "json",
                    success: function (response) {
                        if (response.success) {
                            fileSerial++;
                            $(file.previewTemplate).find(".description-field").attr("name", "description_" + fileSerial);
                            $(file.previewTemplate).append("<input type='hidden' name='file_name_" + fileSerial + "' value='" + file.name + "' />\n\
                                <input type='hidden' name='file_size_" + fileSerial + "' value='" + file.size + "' />");
                            $(file.previewTemplate).find(".file-count-field").val(fileSerial);
                            done();
                        } else {
                            $(file.previewTemplate).find("input").remove();
                            done(response.message);
                        }
                    }
                });
            },
            processing: function () {
                $("#project-file-save-button").prop("disabled", true);
            },
            queuecomplete: function () {
                $("#project-file-save-button").prop("disabled", false);
            },
            fallback: function () {
                //add custom fallback;
                $("body").addClass("dropzone-disabled");
                $('.modal-dialog').find('[type="submit"]').removeAttr('disabled');

                $("#project-file-dropzone").hide();
                $("#project-file-modal-footer").prepend("<button id='add-more-file-button' type='button' class='btn  btn-default pull-left'><i class='fa fa-plus-circle'></i> " + "<?php echo lang("add_more"); ?>" + "</button>");

                $("#project-file-modal-footer").on("click", "#add-more-file-button", function () {
                    var newFileRow = "<div class='file-row pb10 pt10 b-b mb10'>"
                            + "<div class='pb10 clearfix '><button type='button' class='btn btn-xs btn-danger pull-left mr10 remove-file'><i class='fa fa-times'></i></button> <input class='pull-left' type='file' name='manualFiles[]' /></div>"
                            + "<div class='mb5 pb5'><input class='form-control description-field'  name='description[]'  type='text' style='cursor: auto;' placeholder='<?php echo lang("description") ?>' /></div>"
                            + "</div>";
                    $("#project-file-previews").prepend(newFileRow);
                });
                $("#add-more-file-button").trigger("click");
                $("#project-file-previews").on("click", ".remove-file", function () {
                    $(this).closest(".file-row").remove();
                });
            },
            success: function (file) {
                setTimeout(function () {
                    $(file.previewElement).find(".progress-striped").removeClass("progress-striped").addClass("progress-bar-success");
                }, 1000);
            }
        });

                initScrollbar("#project-file-dropzone-scrollbar", {setHeight: 280});

    });



</script>    
  
