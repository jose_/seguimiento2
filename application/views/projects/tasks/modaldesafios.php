<?php echo form_open(get_uri("projects/save_milestonedesafios"), array("id" => "milestone-form", "class" => "general-form", "role" => "form")); ?>

<div class="modal-body clearfix">
    <input type="hidden" name="id" value="<?php echo $model_info->id; ?>" />
    <input type="hidden" name="project_id" value="<?php echo $project_id; ?>" />
    <div class="form-group"style="display:none" >
        <label for="title" class=" col-md-3"><?php echo lang('title'); ?></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "title",
                "name" => "title",
                "value" => $model_info->title,
                "class" => "form-control",
                "placeholder" => lang('title'),
                "maxlength" => "500",
                "autofocus" => true,
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
            ));
            ?>
        </div>
    </div>
                      <img src="../../../assets/images/resultados.png" style="width:200px;height: 200px;" class="center" >

    <div class="form-group">
        <div class=" col-md-15">
           <div> <b>Partiendo de un análisis de contexto ¿Qué aspecto interno como equipo, como organización o como red, dificultaron el trabajo del programa, por qué y cómo lo enfrentaron?</b></div></br>
            <?php
            echo form_textarea(array(
                "id" => "desafios_pregunta1",
                "name" => "desafios_pregunta1",
				//cambiando para este segunda periodo captura
                "value" => $model_info->desafios_pregunta1,
                "class" => "form-control",
                "maxlength" => "500",
                "data-rule-required" => true,
                "data-msg-required" => "No dejar en blanco",
                "placeholder" => "Max 500 caracteres"
            ));
            ?>
        </div>
    </div>
<div class="form-group">
        <div class=" col-md-15">
           <div> <b>Con base en los aprendizajes ya citados ¿Qué elementos de nuestra manera de trabajar y/o de las estrategias tenemos que ajustar o mejorar para lograr tener mayor impacto?</b></div>
            <?php
            echo form_textarea(array(
                "id" => "desafios_pregunta2",
                "name" => "desafios_pregunta2",
                //cambiando para este segunda periodo captura
                "value" => $model_info->desafios_pregunta2,
                "class" => "form-control",
                "data-rule-required" => true,
                "data-msg-required" => "No dejar en blanco",
                "placeholder" => "Max 500 caracteres"
            ));
            ?>
        </div>
    </div>



    <div class="form-group" style="display:none">
        <label for="description" class=" col-md-3"><?php echo lang('description'); ?></label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "description",
                "name" => "description",
                "value" => $model_info->description,
                "class" => "form-control",
                "placeholder" => lang('description')
            ));
            ?>
        </div>
    </div>
    <div class="form-group" style="display:none">
        <label for="due_date" class=" col-md-3"><?php echo lang('due_date'); ?></label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "due_date",
                "name" => "due_date",
                "value" => $model_info->due_date,
                "class" => "form-control",
                "placeholder" => lang('due_date'),
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required")
            ));
            ?>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> <?php echo lang('close'); ?></button>
    <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> <?php echo lang('save'); ?></button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#milestone-form").appForm({
            onSuccess: function (result) {
                $("#milestone-table").appTable({newData: result.data, dataId: result.id});
            }
        });
        $("#title").focus();

        setDatePicker("#due_date");

    });
</script>    