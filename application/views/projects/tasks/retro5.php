<?php echo form_open(get_uri("projects/save_5"), array("id" => "note-form", "class" => "general-form", "role" => "form")); ?>
    <?php 

 

 foreach ($list_data as $lista) { 
            $id=$lista->id;
                $description4= $lista->description4;

              
                    }  


    ?>
<div class="modal-body clearfix">
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <input type="hidden" name="project_id" value="<?php echo $project_id; ?>" />
    <input type="hidden" name="client_id" value="<?php echo $client_id; ?>" />
    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />


      
    <div class="form-group">
        <div class="col-md-12">
            5. ACCIONES. Enlista las principales acciones de mejora que trabajarás durante el año.</br>
            Acción |Fecha de cumplimiento |Medida de éxito |
            <div class="notepad">
                <?php
                echo form_textarea(array(
                    "id" => "description4",
                    "name" => "description4",
                    "value" => $description4,
                    "class" => "form-control",
                    "placeholder" =>  "Acción |Fecha de cumplimiento |Medida de éxito |",
                ));
                
                ?>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> <?php echo lang('close'); ?></button>
    <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> <?php echo lang('save'); ?></button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#note-form").appForm({
            onSuccess: function (result) {
                $("#note-table").appTable({newData: result.data, dataId: result.id});
            }
        });
        $("#title").focus();
        $("#note_labels").select2({
            tags: <?php echo json_encode($label_suggestions); ?>,
            'minimumInputLength': 0
        });
    });
</script>    