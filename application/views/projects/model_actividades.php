<?php echo form_open(get_uri("projects/save_actividades"), array("id" => "note-form", "class" => "general-form", "role" => "form")); ?>
<div class="modal-body clearfix">
    <input type="hidden" name="id" value="<?php echo $model_info->id; ?>" />
            <div class="form-group">


        <label for="otro" class=" col-md-3">Cambio que quieres lograr</label>
         <div class=" col-md-9">

            <?php
            echo form_input(array(
                "id" => "cambio",
                "name" => "cambio",
                "value" => $model_info->cambio,
                "class" => "form-control",
                "placeholder" => "Cambio",
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
            ));
            ?>
        </div>
       </div>
           <div class="form-group">

        
        <label for="description" class=" col-md-3">Activdad a realizar</label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "title",
                "name" => "title",
                "value" => $model_info->title,
                "class" => "form-control",
                "placeholder" => "Escribe la acción que llevaras acabo",
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
                "style" => "height:150px;",
            ));
            ?>
        </div>
    </div>
        <div class="form-group">

  <label for="description" class=" col-md-3">Medida de exito</label>
        <div class=" col-md-9">
            <?php
            echo form_textarea(array(
                "id" => "medida",
                "name" => "medida",
                "value" => $model_info->medida,
                "class" => "form-control",
                "placeholder" => "Medida para lograr el exito",
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
                "style" => "height:150px;",
            ));
            ?>
        </div>
  
 <div class="form-group">


        <label for="title" class=" col-md-3">Fecha de cumplimiento</label>
        <div class=" col-md-9">
            <?php
            echo form_input(array(
                "id" => "fecha",
                "name" => "fecha",
                "value" => $model_info->fecha,
                "class" => "form-control",
                "placeholder" => "Fecha de cumplimiento",
                "autofocus" => true,
                "data-rule-required" => true,
                "data-msg-required" => lang("field_required"),
            ));
            ?>
        </div>

    </div>
    


    <div class="form-group">




<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> <?php echo lang('close'); ?></button>
    <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> <?php echo lang('save'); ?></button>
</div>
 



   

    
</div>


<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#note-form").appForm({
            onSuccess: function (result) {
                $("#note-table").appTable({newData: result.data, dataId: result.id});
            }
        });
        $("#title").focus();
        $("#note_labels").select2({
            tags: <?php echo json_encode($label_suggestions); ?>,
            'minimumInputLength': 0
        });
    });
</script>   

