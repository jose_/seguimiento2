<div id="page-content" class="p20 clearfix">
    <div class="page-title clearfix mb20">
        <h1><?php echo "Evaluación del Desempeño" ?></h1>
        <div class="title-button-group">
            <?php
              
            ?>
        </div>
    </div>
    
    <div class="row">
        <?php foreach ($team_members as $team_member) { ?>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="<?php echo get_avatar($team_member->image); ?>" alt="..."></span> 
                        <h4><?php echo $team_member->first_name . " " . $team_member->last_name; ?></h4>
                        <p class="text-off"><?php echo $team_member->job_title ? $team_member->job_title : "Untitled"; ?></p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <?php echo get_team_member_profile_link($team_member->id, lang("view_details"), array("class" => "btn btn-xs btn-info")); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<h1>REGLAMENTO</h1>
<?php
// echo $num_promotor;
// echo $nombre;
//echo $nombre;

if($intentos == 0)
{
?>
    Muestra las reglas y botón verde para comenzar test...<br/>
    <div class="col-lg-2 center-block">
        <a href="test" class="btn alert-success">COMENZAR</a>
    </div> 
<?php   
}
else {
?>
    Muestra las reglas y botón ROJO DONDE SE INDICA QUE YA NO PUEDE HACER EL TEST... <br/>
        <div class="col-lg-2 center-block">
        <a href="#" class="btn alert-danger"><i class="icon-bar"></i> COMENZAR</a>
    </div> 
<?php
}
?>
<div style="clear: both;"></div>
