<style type="text/css">
    .flot-y1-axis {
        left: -35px !important;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <i class="fa fa-bar-chart pt10"></i>&nbsp; <?php echo lang("chart"); ?>
        <div class="pull-right">

            <div id="expense-chart-date-range-selector">

            </div>
        </div>
    </div>
    <div class="panel-body ">

            <div id="chartdiv" style= "height: 4499px;" ></div>           
    </div>
</div>


<script type="text/javascript">
   


    var prepareExpensesFlotChart = function (data) {
        appLoader.show();
        $.ajax({
            url: "<?php echo_uri("expenses/resultados") ?>",
            data: data,
            cache: false,
            type: 'POST',
            dataType: "json",
            success: function (response) {
                appLoader.hide();
                initExpenseFlotChart(response.expenses,response.data);
            }
        });

    };


    $(document).ready(function () {
        $("#expense-chart-date-range-selector").appDateRange({
            dateRangeType: "yearly",
            onChange: function (dateRange) {
                prepareExpensesFlotChart(dateRange);
            },
            onInit: function (dateRange) {
                prepareExpensesFlotChart(dateRange);
            }
        });

    });
</script>
<script src="http://publicidadoficial.com.mx/treemap/pruebas/amcharts.js" type="text/javascript"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<style>
#chartdiv {
    width       : 100%;
    height      : 500px;
    font-size   : 11px;
}   
.amcharts-chart-div a {display:none !important;}

</style>
            <div id="chartdiv" ></div>           

<script>
    var initExpenseFlotChart = function (expenses,data) {
        console.log(expenses["0"]);
                console.log(data["0"]);

AmCharts.addInitHandler( function( chart ) {

  // Check if `orderByField` is set
  if ( chart.orderByField === undefined ) {
    // Nope - do nothing
    return;
  }

  // Re-order the data provider
  chart.dataProvider.sort( function( a, b ) {
    if ( a[ chart.orderByField ] > b[ chart.orderByField ] ) {
      return -1;
    } else if ( a[ chart.orderByField ] == b[ chart.orderByField ] ) {
      return 0;
    }
    return 1;
  } );

}, [ "serial" ] );
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
     "theme": "light",
    "orderByField": "expenses",
    "categoryField": "years",
    "rotate": true,
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start",
        "position": "left"
    },
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[year]]</br><b>Mi evaluación:</b>[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-1",
            "lineAlpha": 0.2,
            "title": "Income",
            "type": "column",
            "valueField": "income"
        },
        {
            "balloonText": "[[year]]</br> <b>Mi promedio de panel de evaluadores: </b>[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-2",
            "lineAlpha": 0.2,
            "title": "Expenses",
            "type": "column",
            "valueField": "expenses"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "position": "top",
            "axisAlpha": 0
        }
    ],

    "allLabels": [],
    "balloon": {
    "borderThickness": 3,
    "horizontalPadding": 17,
    "offsetX": 50,
    "offsetY": 8},
    "titles": [],
    "dataProvider": [
        {
            "year":"Pregunta 1. <b>Prestar atención</b> a nuestros(as) interlocutores(as) <b>para comprender</b> sus planteamientos (pensamientos, sentimientos e intereses) <b>es fundamental para el trabajo colectivo.</b> ",
            "years":"1",
            "income": expenses["0"]["0"],
            "expenses": data["0"]["0"]
        },
        {
            "year": "Pregunta 2. <b>Escuchar con atención y valorar todas las opiniones</b> son dos elementos clave para el intercambio de ideas.   ",
            "years":"2",
           "income": expenses["1"]["0"],
            "expenses": data["1"]["0"]
        },
        {
            "year": "Pregunta 3. <b>Proponer ideas </b>para atender problemáticas <b>desde distintas opciones,</b> incrementa la posibilidad de mejores resultados.  ",
            "years":"3",
            "income": expenses["2"]["0"],
            "expenses": data["2"]["0"]
        },
        {
            "year": "Pregunta 4.<b> Lograr acuerdos en donde distintas partes queden conformes</b> es el resultado de una negociación efectiva.   ",
            "years":"4",
           "income": expenses["3"]["0"],
            "expenses": data["3"]["0"]
        },
        {
            "year": "Pregunta 5. Trabajar en equipo requiere <b>saber dialogar,</b> y en su caso, <b>mediar discusiones hasta generar puntos en común.</b>   ",
            "years":"5",
            "income": expenses["4"]["0"],
            "expenses": data["4"]["0"]
        },
        {
            "year": "Pregunta 6. Recibir con <b>buena disposición</b> las propuestas y/o <b>experiencias de los(as) otros(as)</b> significa ser receptivo(a).  ",
            "years":"6",
            "income": expenses["5"]["0"],
            "expenses": data["5"]["0"]
        },
        {
            "year": "Pregunta 7. <b>Reconocer y analizar las aportaciones</b> de los demás y <b>valorar su pertinencia para la toma de decisiones,</b> representa un ejercicio incluyente en donde<b> todos los puntos de vista tienen cabida.</b>  ",
            "years":"7",
            "income": expenses["6"]["0"],
            "expenses": data["6"]["0"]
        },
        {
            "year": "Pregunta 8. Desarrollar las actividades laborales cotidianas dentro de <b>marco de respeto y confianza</b> es altamente valorado para el clima laboral en las organizaciones,  ",
            "income": expenses["7"]["0"],
            "years":"8",
            "expenses": data["7"]["0"]
        },
        {
            "year": "Pregunta 9. <b>Escuchar y entender</b> a los y las demás, intentando <b>comprender sus sentimientos, intereses y preocupaciones,</b> aun cuando estos no se hagan explícitos en su totalidad, es parte de ser empático(a).    ",
            "income": expenses["8"]["0"],
            "years":"9",
            "expenses": data["8"]["0"]
        },
        {
            "year": "Pregunta 10. Pensar y realizar acciones con base en <b>valores, principios y prácticas éticas, respetando las políticas institucionales,</b> son determinantes de una conducta honesta.  ",
            "income": expenses["9"]["0"],
            "years":"10",
            "expenses": data["9"]["0"]
        },
        {
            "year": "Pregunta 11. Afrontar y <b>saberse adaptar positivamente a situaciones adversas</b> (errores, riesgos, amenazas, estrés, etc.) es parte de ser resiliente.   ",
            "income": expenses["10"]["0"],
            "years":"11",
            "expenses": data["10"]["0"]
        },
        {
            "year": "Pregunta 12. El tratamiento de un tema requiere de <b>conocimientos y capacidades adquiridas</b> durante la formación académica al tiempo que se complementan con la<b> experiencia profesional.</b>   ",
            "income": expenses["11"]["0"],
            "years":"12",
            "expenses": data["11"]["0"]
        },
        {
            "year": "Pregunta 13. <b>La capacitación y la actualización constantes</b> reflejan el interés por incrementar los conocimientos, habilidades y saberes respecto de un tema. ",
            "income": expenses["12"]["0"],
            "years":"13",
            "expenses": data["12"]["0"]
        },
        {
            "year": "Pregunta 14.<b> Generar nuevas ideas e impulsar propuestas</b> originales, creativas, con <b>viabilidad técnica-política y acorde a con los principios institucionales,</b> son acciones altamente valoradas en ambientes institucionales eficientes.  ",
            "income": expenses["13"]["0"],
            "years":"14",
            "expenses": data["13"]["0"]
        },
        {
            "year": "Pregunta 15. Ser constante en los emprendimientos y<b> mantener estabilidad y continuidad en las acciones,</b> son condiciones de la perseverancia.   ",
            "income": expenses["14"]["0"],
            "years":"15",
            "expenses": data["14"]["0"]
        },
        {
            "year": "Pregunta 16. Adecuarse a los cambios y, si fuese necesario, y<b> modificar conductas para alcanzar resultados</b> es parte de la adaptación.   ",
            "income": expenses["15"]["0"],
            "years":"16",
            "expenses": data["15"]["0"]
        },
        {
            "year": "Pregunta 17. Valorar y <b>reconocer el esfuerzo, los logros y los méritos de las y los demás,</b> incrementa el compañerismo y genera ambientes armónicos en las instituciones.  ",
            "income": expenses["16"]["0"],
            "years":"17",
            "expenses": data["16"]["0"]
        },
        {
            "year": "Pregunta 18. <b>Cumplir con los compromisos y acuerdos</b> fortalece la eficiencia en las instituciones y <b>genera confianza</b> para el trabajo colaborativo.  ",
            "income": expenses["17"]["0"],
            "years":"18",
            "expenses": data["17"]["0"]
        },
        {
            "year": "Pregunta 19. Brindar aportaciones claras y constructivas e interesarse genuinamente por los aprendizajes y desafíos que otras y otros han identificado respecto de un tema, son elementos clave de una retroalimentación efectiva.  ",
            "income": expenses["18"]["0"],
            "years":"19",
            "expenses": data["18"]["0"]
        },
        {
            "year": "Pregunta 20. Hacer<b> buen uso de los recursos institucionales</b> (de todo tipo) incrementa la eficiencia, la transparencia, la rendición de cuentas y la confianza en las organizaciones. Al respecto, la o el integrante del equipo:",
            "income": expenses["19"]["0"],
            "years":"20",
            "expenses": data["19"]["0"]
        },
        {
            "year": "Pregunta 21. Las habilidades de <b>expresar ideas tanto de manera verbal como de forma escrita, y con la claridad necesaria,</b> complementan la capacidad de análisis.",
            "income": expenses["20"]["0"],
            "years":"21",
            "expenses": data["20"]["0"]
        },
        {
            "year": "Pregunta 22. El <b>uso eficiente de los datos, a partir del conocimiento y uso de métodos para su sistematización y análisis,</b> contribuye a robustecer nuestras investigaciones y los productos que se generan a partir de éstas.  ",
            "income": expenses["21"]["0"],
            "years":"22",
            "expenses": data["21"]["0"]
        },
        {
            "year": "Pregunta 23. Tener conocimiento y<b> hacer uso de métodos probados para el análisis cualitativo y cuantitativo </b>incrementa la validez y robustez de las investigaciones.  ",
            "income": expenses["22"]["0"],
            "years":"23",
            "expenses": data["22"]["0"]
        },
        {
            "year": "Pregunta 24. <b>Desagregar un problema o una situación en sus diferentes elementos</b> para comprenderlos y analizarlos de manera exhaustiva, así como identificar sus relaciones causales, son prácticas que dan cuenta de nuestra capacidad de reflexión.    ",
            "income": expenses["23"]["0"],
            "years":"24",
            "expenses": data["23"]["0"]
        },
        {
            "year": "Pregunta 25. Saber <b>comunicar y ordenar nuestras ideas de manera clara, concisa y contundente,</b> así como plantear mensajes potentes y significativos son características valiosas para nuestros procesos internos y de incidencia.  ",
            "income": expenses["24"]["0"],
            "years":"25",
            "expenses": data["24"]["0"]
        },
        {
            "year": "Pregunta 26. Prestar atención e implementar estrategias para<b> atender tanto la forma (presentación) como al contenido (análisis y evidencia)</b> de nuestros productos y plataformas de fortalecimiento institucional, de investigación y/o de incidencia, incrementa su calidad.   ",
            "income": expenses["25"]["0"],
            "years":"26",
            "expenses": data["25"]["0"]
        },
        {
            "year": "Pregunta 27. <b>Reconocer puntualmente aquellas conductas y/o situaciones que pueden dificultar o conflictuar el trabajo, </b>contribuye a brindarles atención oportunamente y resolver las problemáticas de manera adecuada.   ",
            "income": expenses["26"]["0"],
            "years":"27",
            "expenses": data["26"]["0"]
        },
        {
            "year": "Pregunta 28.<b> Analizar, poner en perspectiva, y solicitar ayuda</b> frente a una situación y/o una conducta compleja, contribuye a que las opciones de atención y solución sean más efectivas.  ",
            "income": expenses["27"]["0"],
            "years":"28",
            "expenses": data["27"]["0"]
        },
        {
            "year": "Pregunta 29. Identificar los momentos, situaciones y/o conductas frente a los cuales se debe tomar una decisión, así como <b>analizar las alternativas, priorizarlas y decidir cuál podría tener mejor resultados </b>(buscando asesoría si se requiere), son condiciones que incrementan la efectividad de las resoluciones.  ",
            "income": expenses["28"]["0"],
            "years":"29",
            "expenses": data["28"]["0"]
        },
        {
            "year": "Pregunta 30. Identificar escenarios de éxito y (re) <b>ajustar la energía, el esfuerzo y los recursos a partir de acciones orientas a resultados,</b> son características de la visión estratégica y la orientación a resultados. Al respecto la o el integrante de equipo:   ",
            "income": expenses["29"]["0"],
            "years":"30",
            "expenses": data["29"]["0"]
        },
        {
            "year": "Pregunta 31. <b>Analizar las alternativas u opciones</b> para atender determinado compromiso, situación o conducta,<b> y ordenarlas respecto de su viabilidad y beneficios,</b> nos ayuda a dar prioridad a los asuntos en función de nuestras competencias y capacidades.  ",
            "income": expenses["30"]["0"],
            "years":"31",
            "expenses": data["30"]["0"]
        },
        {
            "year": "Pregunta 32. <b>Administrar de forma eficaz y eficiente el tiempo disponible</b> en función de las actividades y procesos a ejecutar, y a la luz de los objetivos propuestos y resultados esperados, abona a una gestión efectiva del trabajo.   ",
            "income": expenses["31"]["0"],
            "years":"32",
            "expenses": data["31"]["0"]
        },
        {
            "year": "Pregunta 33. Establecer objetivos puntuales que den sentido a los procesos, así como<b> agrupar diversas actividades sustantivas que se orienten al cumplimiento de resultados concretos,</b> son acciones que abonan a la eficacia para la gestión del trabajo.   ",
            "income": expenses["32"]["0"],
            "years":"33",
            "expenses": data["32"]["0"]
        },
        {
            "year": "Pregunta 34. Proponer y <b>fomentar conductas y acciones que motiven el interés, </b>fortalezcan el <b>compromiso</b> de las y los demás y repercutan de manera positiva en la eficiencia y eficacia del trabajo, son acciones que componen el liderazgo. Al respecto la y el integrante del equipo:  ",
            "income": expenses["33"]["0"],
            "years":"34",
            "expenses": data["33"]["0"]
        },
        {
            "year": "Pregunta 35. En procesos colaborativos, <b>identificar las competencias y las capacidades con las que cuentas las y los demás y pedirles ayuda</b> para desarrollar determinadas actividades, abona a fortalecer los resultados y el rendimiento del trabajo.  ",
            "income": expenses["34"]["0"],
            "years":"35",
            "expenses": data["34"]["0"]
        }
    ],
    "export": {
        "enabled": true
     }

});
}
</script>