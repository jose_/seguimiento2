<style>
    #finalizado {
    cursor: pointer;
    min-width: 28px;
    background: #fff;
    border-radius: 50%;
    display: inline-block;
    position: relative;
    vertical-align: central;
    text-align: center;
    margin: 0 5px;
    padding: 4px 0px;
    border: 1px solid #e2e7f1;
    background: #2ac2d4;
    color: #fff;
    border: 1px solid #2ac2d4;
}
</style>
<?php  


          foreach ($promedio_total_grupo as $pro) { 

              $eltotal_grupo= $pro->promedio;
                    }  
          
foreach ($promedio_total_yo as $pro_yo) { 

              $eltotal_yo= $pro_yo->promedio;
                    }  
          

                              foreach ($notas as $not) { 

               $titulo_notas= $not->title;
               $descripcion_notas= $not->description;
                              $descripcion_notas2= $not->description2;
               $descripcion_notas3= $not->description3;
               $descripcion_notas4= $not->description4;

              
                    }  

                ?>
        <?php foreach ($modulomio as $mod) { 

  $modulo_validar_mio= $mod->modulo_status;
}


     foreach ($get_suma_modulos as $sum) { 

  $suma_evaluadores= $sum->total;
}
 $suma_evaluadores;
 ?>

<div id="page-content" class="p20 clearfix">

    <div class="titulo">
        <h1><?php echo "Viendo la evaluación del desempeño de mis colegas"; ?></h1>
        
    </div>
    <div class="box">
    <div class="box-content">
        <div id="timeline-content" class="clearfix p15 mb20">
   <div class="row">
            <div class="titulo"></div>


        <?php foreach ($team_members as $team_member) { ?>
            <div class="col-md-3 col-sm-6"  style="padding-left:20px !important">
                <div class="panel panel-default  text-center ">
                      <div class="panel-footer bg-info p15 no-border">
                        <?php //echo get_team_member_panel_link($team_member->id, lang("evaluarme"), array("class" => "btn btn-xs btn-info")); ?>
                    </div>
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="<?php echo get_avatar($team_member->image); ?>" alt="..."></span> 
						<h4><?php echo $evaluado=$team_member->first_name . " " . $team_member->last_name; ?></h4>
                        <p class="text-off"><?php echo $team_member->job_title ? $team_member->job_title : "Untitled"; ?></p>

                    </div>
                  
                    <?php  if ($suma_evaluadores >=21) {?> 
                <div class="btn btn-xs btn-info">
                    
                 </div>
                              
                <?php } ?>
             </div>
            
            </div>
        <?php } ?>

<div id="page-content" class="clearfix p20">
    <div class="panel clearfix">
    <h4 class="pl10">Los resultados</h4>    
        <ul data-toggle="ajax-tab" class="nav nav-tabs bg-white inner p10" role="tablist">
         
            <?php  if ($suma_evaluadores>=21) {?>    
         <div style=" padding: 10px;">
        <h2>Promedio  del panel de sus evaluadores es: <b><?php echo round($eltotal_grupo, 2); ?></b></h2></div>
     <div style="width:800px;"> 
        <h2>El promedio de su autoevaluación: <b><?php echo round($eltotal_yo, 2); ?></b></h2></div>
    
     <div style="width:800px; padding-left: 300px;"> 
     <b>1. Piensa en los objetivos de tu puesto y ¿ cuáles serían los ajustes que deberías realizar para lograr un desempeño exitoso?</b></br>
        <?php echo $titulo_notas;?></br>
      <b> 2. ¿Qué te sorprendió de los resultados?</b></br>
            <?php echo $descripcion_notas;?></br>
           <b> 3. BRECHAS MÁS SIGNIFICATIVAS. Identifica los comportamientos con mayor brecha entre tu percepción y la percepción de los otros.</b></br>
             <?php echo $descripcion_notas2;?></br>
            <b> 4. CAMBIOS. Identifica los comportamientos que quisieras cambiar para tu mejor desempeño.</b></br>
             <?php echo $descripcion_notas3;?></br>
             <b>5. ACCIONES. Enlista las principales acciones de mejora que trabajarás durante el año.</br></b>
Acción |Fecha de cumplimiento |Medida de éxito |</br>
            <?php //echo $descripcion_notas4;?></br>



</div>

            </li>
         <?php } else {?> 
           <div style=" padding: 40px;"><h2>Bienvenidos: </b></h2>Ahora podrás usar el sistema para tener una visión amplia y amigable.</div>
            </li>
            <?php }?> 
         
            <div class="pull-right pr10">
                <div id="yearly-chart-date-range-selector" style="display:none">
                </div>
            </div>
                   

       
        </ul>

        <div class="tab-content p15">
           <?php  
 if($modulo_validar_mio>=2){
            ?>

            <div id="income-vs-expenses-chart" style="width: 100%; height: 350px;"></div>

        <?php }?>
        </div>
    </div>
</div>

<?php
load_js(array(
    "assets/js/flot/jquery.flot.min.js",
    "assets/js/flot/jquery.flot.resize.min.js",
    "assets/js/flot/jquery.flot.tooltip.min.js",
    "assets/js/flot/curvedLines.js",
));
?>
<script>
  var year = <?php echo json_encode($year); ?>;
</script>
<script type="text/javascript">
    var initIncomeExpenseChart = function (income, expense) {
        var dataset = [
            {
                data: income,
                color: "rgba(0, 179, 147, 1)",
                lines: {
                    show: true,
                    fill: 0.2
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                label: "Tu promedio",
                data: income,
                color: "rgba(0, 179, 147, 1)",
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    fill: true,
                    radius: 4,
                    fillColor: "#fff",
                    lineWidth: 1
                },
                shadowSize: 0,
                curvedLines: {
                    apply: false
                }
            },
            {
                data: expense,
                color: "#F06C71",
                lines: {
                    show: true,
                    fill: 0.2
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                label: "Promedio de equipo",
                data: expense,
                color: "#F06C71",
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    fill: true,
                    radius: 4,
                    fillColor: "#fff",
                    lineWidth: 1
                },
                shadowSize: 0,
                curvedLines: {
                    apply: false
                }
            }

        ];
        $.plot("#income-vs-expenses-chart", dataset, {
            series: {
                curvedLines: {
                    apply: true,
                    active: true,
                    monotonicFit: true
                }
            },
            legend: {
                show: true
            },
            yaxis: {
                min: 0,
                max:5
            },
            xaxis: {
                ticks: [[1, "COMUNICACIÓN ASERTIVA"],[2, "RESPETO A LA DIVERSIDAD "],[3, "PROFESIONALISMO Y RESPONSABILIDAD "],[4, "HABILIDADES ANALÍTICAS"],[5, "HABILIDADES RESOLUTIVAS"], [6, "HABILIDADES DE GESTIÓN"]]
            },
            grid: {
                color: "#bbb",
                hoverable: true,
                borderWidth: 0,
                backgroundColor: '#FFF'
            },
            tooltip: {
                show: true,
                content: function (x, y, z) {
                    if (x) {
                        return "%s: " + (z);
                    } else {
                        return false;
                    }
                },
                defaultTheme: false
            }
        });
    };

    var prepareExpensesFlotChart = function (data) {
        appLoader.show();
        $.ajax({
            url: "<?php echo_uri("expenses/promedios2") ?>",
            data: {year: year, name: name},
            cache: false,
            type: 'POST',
            dataType: "json",
            success: function (response) {
                appLoader.hide();
                initIncomeExpenseChart(response.income, response.expenses);
            }
        });
    };
    $(document).ready(function () {

        $("#yearly-chart-date-range-selector").appDateRange({
            dateRangeType: "yearly",
            onChange: function (dateRange) {
                prepareExpensesFlotChart(dateRange);
            },
            onInit: function (dateRange) {
                prepareExpensesFlotChart(dateRange);
            }
        });
    });

</script>

    </div>




            </div>
    </div>
    <div class="hidden-xs box-content bg-white" style="width: 250px; min-height: 100%;">
        <div id="user-list-container" >
            <div class="p15">           
			<div class="titulo"><b>Los Evaluadores de <?php echo $evaluado;?> </b></div>

                <?php
                foreach ($quienevalua as $evaluacion2) {
                    ?>
                    <div class="media">
                        <div class="media-left">
                            <span class="avatar avatar-xs">
								<img src="<?php echo get_avatar($evaluacion2->image); ?>" alt="..." />
                            </span>
                        </div>
                        <div class="media-body clearfix w100p">
                            <div class="media-heading pull-left m0">
                                <div class="w150"><?php echo get_team_member_profile_link($member->id, $evaluacion2->first_name . " " . $evaluacion2->last_name, array("class" => "dark")); ?> <?php  $terminado= $evaluacion2->modulo_status; if($terminado>=7){?><p class="finalizado" id="finalizado" ><i class="fa fa-check"></i></p><?php }?> </div>
                                <small class="text-off"><?php echo $evaluacion2->job_title; ?></small>
                                             
                            </div>
                            <div class="pull-right">
                                <?php
                                if (get_setting("module_message")) {
                                    echo modal_anchor(get_uri("messages/modal_form/" . $evaluacion2->id), "<i class='fa fa-envelope-o'></i>", array("class" => "btn btn-default btn-xs round", "title" => lang('send_message')));
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
                 <?php 
            //validamos que existan comentarios y los mostramos
        if ($comentarios<>null) {    
         if($suma_evaluadores >=21 ){    ?>
                 <div class="row">
            <div class="p15"> <b> Comentarios de mis evaluadores:</b></div>

            
        <?php foreach ($comentarios as $comentario) { ?>

                <div class="panel">
                    <div class="panel-body">

                        <?php echo $comentario->respuesta; ?>                
                    </div>
                  
            </div>
        <?php } ?>
    </div>

    <?php }}?>
            </div>
        </div>
    </div>
</div>






</div>
