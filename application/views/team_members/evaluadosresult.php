<div id="page-container" class="box-content">
                
                <div class="scrollable-page mCustomScrollbar _mCS_1 mCS-autoHide" style="height: 703px; position: relative; overflow: visible;"><div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px;" dir="ltr">
                    <div id="page-content" class="p20 clearfix">
    <div class="page-title clearfix mb20">
        <h1>Equipo Fundarita</h1>
     
    </div>
    
    <div class="row">
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 

                        <h4> Arelhí  Galicia</h4>
                        <p class="text-off">Asistente de dirección ejecutiva</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(40); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    

                <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Alicia Ojeda</h4>
                        <p class="text-off">Analista</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(43); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Diana Amador</h4>
                        <p class="text-off">Cordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(44); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>


<div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a38420729937-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Francisco Jasso</h4>
                        <p class="text-off">Soporte tecnico</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(9); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Francisco José Reynoso Arreola</h4>
                        <p class="text-off">Investigador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(45); ?> class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>

                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a383a438ed6b-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Anaid  Garcia</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(6); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a395b4f14ba3-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Andrés Marcelo Díaz Fernández</h4>
                        <p class="text-off">Investigador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(37); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a3c08c766155-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Aroa  de la Fuente </h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(14); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a5533b43d5ab-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Beatriz Olivera</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(17); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a393f2c82335-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Cecile  Lachenal</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(25); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a3a8df340984-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Cecilia Toledo</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(26); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file593b204f81b8e-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Claudia de Anda</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(19); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a3948e32c738-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Daniela Francisca Díaz Echeverría</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(27); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Edmundo Del Pozo</h4>
                        <p class="text-off">Coordinador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(16); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5944071711cf0-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Eduardo Alcalá</h4>
                        <p class="text-off">Coordinador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(34); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file594325bd65977-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Federico Ramirez</h4>
                        <p class="text-off">Coordinador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(7); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Felipe  Varela</h4>
                        <p class="text-off">Responsable Administrativo</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(2); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                 
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a393ef19a37d-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Gina  Iliana  Chacón Fregoso</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(5); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5940add9971a9-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Guillermo Ávila</h4>
                        <p class="text-off">Investigador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(22); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Haydeé Pérez Garrido</h4>
                        <p class="text-off">Directora Ejecutiva</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(39); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Humberto Guerrero</h4>
                        <p class="text-off">Coordinador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(35); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a60db84e3b33-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Ingrid Picasso</h4>
                        <p class="text-off">Analista</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(41); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Itzel Silva Monroy</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(18); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file593b69df2659a-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Itzi Salgado</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(30); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Iván Benumea </h4>
                        <p class="text-off">Investigador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(13); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file593ec26c99f51-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Janet Oropeza</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(23); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a36dbcce7eb1-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>José Chavez</h4>
                        <p class="text-off">Desarrollador Sr</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(29); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a383be664f0e-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Jose Knippen</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(38); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a54008c46bdc-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Juliana Martínez Nacarato</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(42); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a3852ebbc073-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Justine Dupuy</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(21); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a3844f19f160-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Kevin Martinez</h4>
                        <p class="text-off">Desarrollador jr</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(8); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file593ed1b7aed14-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Marco Partida</h4>
                        <p class="text-off">Responsable de comunicación gráfica</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(32); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a5643adf3f3f-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Mariana González</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(15); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5935c4ec31f7f-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Marine Perron</h4>
                        <p class="text-off">Analista</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(20); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Matilde Pérez</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(28); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Patricia Cabanzo</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(3); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a53b89b02acc-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Paulina Castaño</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(11); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Regina Ganem</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(33); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Renata  Terrazas</h4>
                        <p class="text-off">Coordinadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(4); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/files/profile_images//_file5a385792b3502-avatar.png" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Sarahí Salvatierra</h4>
                        <p class="text-off">Investigadora</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(24); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
             
                    <div class="col-md-3 col-sm-6">
                <div class="panel panel-default  text-center ">
                    <div class="panel-body">
                        <span class="avatar avatar-md mt15"><img src="http://seguimiento.fundarlabs.mx/assets/images/avatar.jpg" alt="..." class="mCS_img_loaded"></span> 
                        <h4>Ximena Antillón</h4>
                        <p class="text-off">Investigador</p>
                    </div>
                    <div class="panel-footer bg-info p15 no-border">
                        <a href="http://seguimiento.fundarlabs.mx/index.php/cuestionario/evaluados/<?php echo base64_encode(36); ?>" class="btn btn-xs btn-info">Ver Detalles</a>                    </div>
                </div>
            </div>
            </div>
</div>                </div></div></div>
            </div>