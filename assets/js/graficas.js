function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


var initIncomeExpenseChart = function (income, expense) {
        var dataset = [
            {
                data: income,
                color: "#ffe319",
                lines: {
                    show: true,
                    fill: 0.1
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                label: "Tu promedio",
                data: income,
                color: "#ffba00",
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    fill: true,
                    radius: 4,
                    fillColor: "#fff",
                    lineWidth: 1
                },
                shadowSize: 0,
                curvedLines: {
                    apply: false
                }
            },
            {
                data: expense,
                color: "#8d56a3",
                lines: {
                    show: true,
                    fill: 0.1
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                label: "Promedio de equipo",
                data: expense,
                color: "#F06C71",
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    fill: false,
                    radius: 4,
                    fillColor: "#fff",
                    lineWidth: 1
                },
                shadowSize: 0,
                curvedLines: {
                    apply: false
                }
            }

        ];
        $.plot("#income-vs-expenses-chart", dataset, {
            series: {
                curvedLines: {
                    apply: true,
                    active: true,
                    monotonicFit: true
                }
            },
            legend: {
                show: true
            },
            yaxis: {
                min: 0,
                max:5
            },
            xaxis: {
                ticks: [[1, "COMUNICACIÓN ASERTIVA"],[2, "RESPETO A LA DIVERSIDAD "],[3, "PROFESIONALISMO Y RESPONSABILIDAD "],[4, "HABILIDADES ANALÍTICAS"],[5, "HABILIDADES RESOLUTIVAS"], [6, "HABILIDADES DE GESTIÓN"]]
            },
            grid: {
                color: "#bbb",
                hoverable: true,
                borderWidth: 0,
                backgroundColor: '#FFF'
            },
            tooltip: {
                show: true,
                content: function (x, y, z) {
                    if (x) {
                        return "%s: " + (z);
                    } else {
                        return false;
                    }
                },
                defaultTheme: false
            }
        });
    };









